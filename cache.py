import json
import os

path = os.getcwd() + '/cache/'

class Session():
    def __init__(self, session, jsessionid, contextid):
        self.session_data = self._session_data()
        self.session_data['jsessionid'] = jsessionid
        self.session_data['contextid'] = contextid
        self.jsessionid = jsessionid

        self.file_path = path+self.jsessionid
        if os.path.exists(self.file_path) != True:
            self.file = open( path + jsessionid, 'w')
            self.file.write(json.dumps(self.session_data))
            self.file.close()
            

    def add_data(self, key, data):
        self.file = open( path + self.jsessionid, 'r')
        self.cache = json.load(self.file)
        self.file.close()
        self.cache[key] = data
        self.file = open( path + self.jsessionid, 'w')
        self.file.write(json.dumps(self.cache))
        self.file.close()

    def get_data(self):
        self.file = open( path + self.jsessionid, 'r')
        self.cache = json.load(self.file)
        self.file.close()
        return self.cache

    def delete_data(self, key):
        self.file = open( path + self.jsessionid, 'r')
        self.cache = json.load(self.file)
        self.file.close()
        del self.cache[key]
        self.file = open( path + self.jsessionid, 'w')
        self.file.write(json.dumps(self.cache))
        self.file.close()

    def _session_data(self):
        return {
            'contextid':'',
            'jsessionid':'',
            'session_obj': '',
            'cookies':[],
            'contextid':[],
            'journey':[]
        }