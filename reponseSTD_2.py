import util
import copy


class Fare():
    def __init__(self, parent_fares, data):
        self.__parent_segment = parent_fares and parent_fares.segment or False
        self.__parent_fares = parent_fares
        self.__dict__ = self._default()
        self.update(data)
        self.service_charges = ServiceCharges(self)


    def _default(self):
        return {
            'sequence': self.__parent_fares and len(self.__parent_fares) or 0,
            'fare_code': '',
            'class_of_service': 'Y',
            'subclass': '',
            'available_count': 0,
            'rule_number': '',
            'product_class': '',
            'product_class_description': '',
            'service_charges': [],
            'base_fare': 0,
            'fare_basis_codes': [],
            'currency': 'IDR'
        }

    def __nonzero__(self):
        return self.fare_code and True or False

    def update(self, new_data):
        self.__dict__.update(new_data)
        # if not self.__dict__['segment_code']:
        #     self.__dict__['segment_code'] = util.segment_attr_to_code(self.__dict__, self.__dict__['provider'])
        #
        # self.__dict__['operating_airline_code'] = self.__dict__.get('operating_airline_code', self.__dict__['carrier_code'])

    def __compute(self):
        for rec in self.service_charges:
            if rec.pax_type in ['ADT'] and rec.charge_code in ['fare']:
                self.__dict__['base_fare'] = rec.amount

    def to_dict(self):
        self.__compute()
        res = self.__dict__.copy()
        res['service_charges'] = self.service_charges.to_dict()
        return res

    @property
    def segment(self):
        return self.__parent_segment

    @property
    def fares(self):
        return self.__parent_fares

    def add_service_charge(self, data={}):
        return self.service_charges.add_service_charge(data)


class Fares():
    def __init__(self, parent_segment):
        self.__parent_segment = parent_segment
        self._fares = []

    def __iter__(self):
        return iter(self._fares)

    def __nonzero__(self):
        return self._fares and True or False

    def __len__(self):
        return len(self._fares)

    def add_fare(self, data={}):
        _new = Fare(self, data)
        self._fares.append(_new)
        return _new

    def __compute(self):
        self._fares = sorted(self._fares, key=lambda r: r.base_fare)
        # last_seg = None
        # for seg in self._fares:
        #     if last_seg:
        #         seg.transit_duration = util.calculate_transit_duration(last_seg.arrival_date, seg.departure_date)
        #     last_seg = seg
        # pass

    @property
    def count(self):
        return len(self._fares)

    @property
    def segment(self):
        return self.__parent_segment

    def to_dict(self):
        self.__compute()
        return [fare.to_dict() for fare in self._fares]


class Leg():
    def __init__(self, parent_legs, data):
        self._parent_legs = parent_legs
        self.__dict__ = self._default()
        self.update(data)

    @property
    def legs(self):
        return self._parent_legs

    def _default(self):
        return {
            'sequence': 0,
            'carrier_code': '',
        }

    def __nonzero__(self):
        return self.carrier_code and True or False

    def update(self, new_data):
        self.__dict__.update(new_data)
        # if not self.__dict__['segment_code']:
        #     self.__dict__['segment_code'] = util.segment_attr_to_code(self.__dict__, self.__dict__['provider'])
        #
        # self.__dict__['operating_airline_code'] = self.__dict__.get('operating_airline_code', self.__dict__['carrier_code'])

    def __compute(self):
        pass

    def to_dict(self):
        self.__compute()
        return self.__dict__


class Legs():
    def __init__(self, parent_segment):
        self.__parent_segment = parent_segment
        self._legs = []

    def __iter__(self):
        return iter(self._legs)

    def __nonzero__(self):
        return self._legs and True or False

    @property
    def segment(self):
        return self.__parent_segment

    def add_leg(self, data={}):
        _new = Leg(self, data)
        self._legs.append(_new)
        _new.sequence = len(self._legs) - 1
        return _new

    def __compute(self):
        # legs = self._legs
        # for idx, leg in enumerate(legs):
        #     if not idx == 0:
        #         leg.update({
        #             'transit_duration': util.calculate_transit_duration(leg[idx - 1]['arrival_date'],
        #                                                                 leg['departure_date'])
        #         })
        pass

    @property
    def count(self):
        return len(self._legs)

    def to_dict(self):
        self.__compute()
        return [rec.to_dict() for rec in self._legs]


class Journey():
    def __init__(self, parent_journeys, data):
        self.__parent_journeys = parent_journeys
        self.__dict__ = self.__default()
        self.segments = Segments(self)
        self.update(data)

    def __default(self):
        return {
            'sequence': self.__parent_journeys and len(self.__parent_journeys._journeys) or 0,
            'journey_type': 'DP',
            'journey_code': '',
            'provider': '',
            'origin': '',
            'destination': '',
            'departure_date': '',
            'arrival_date': '',
            'return_date': '',
            'IncludesTaxesAndFees': '',
            'transit_count': 0,
            'service_charge_summary': [],
            'sale_service_charge_summary': [],
            'combo_price': False,
            'carrier_code_list': set()  # at the end of process, we convert to list
        }

    def __nonzero__(self):
        return self.origin and True or False

    def update(self, new_data):
        self.__dict__.update(new_data)

    def add_segment(self, data):
        self.carrier_code_list = self.carrier_code_list.union([data['carrier_code']])
        return self.segments.add_segment(data)

    def __compute(self):
        # =============== segment : elapsed_time, transit_duration ==================
        last_seg = None
        return_seg = None
        arrival_seg = None
        for seg in self.segments:
            # Journey COde Process
            self.journey_code = ';'.join((self.journey_code, seg.segment_code))

            if last_seg:
                # Fixme : untuk Sabre/Promo_price = promo journye perlu perhatikan, krn dalam satu journey(DP) terdiri dari DP & RT
                if self.combo_price:
                    if last_seg.journey_type != seg.journey_type and last_seg.journey_type == 'DP':
                        return_seg = seg
                        arrival_seg = last_seg
            last_seg = seg
        self.journey_code = self.journey_code[1:]

        # =============== journey : transit_count, departure_date, arrival_date, elapsed_time ========
        if self.segments:
            self.__dict__.update({
                'origin': self.segments[0].origin,
                'destination': self.segments[-1].destination,
                'transit_count': len(self.segments) - 1,
                'departure_date': self.segments[0].departure_date,
                'arrival_date': self.segments[-1].arrival_date,
            })
        # 'Jika combo_price & ada return segment
        if return_seg:
            self.__dict__.update({
                'destination': arrival_seg.destination,
                'transit_count': arrival_seg.sequence,
                'arrival_date': arrival_seg.arrival_date,
                'return_date': return_seg.departure_date,
            })

            # carrier_code_list : dict to list

    def to_dict(self):
        self.__compute()
        res = self.__dict__.copy()
        res['carrier_code_list'] = list(self.carrier_code_list)  # Convert to List
        res['segments'] = self.segments.to_dict()
        return res

    @property
    def journeys(self):
        return self.__parent_journeys

class Journeys():
    def __init__(self):
        self._journeys = []

    def __iter__(self):
        return iter(self._journeys)

    def __nonzero__(self):
        return self._journeys and True or False

    def __len__(self):
        return len(self._journeys)

    def add_journey(self, data={}):
        _new = Journey(self, data)
        self._journeys.append(_new)
        return _new

    def __compute(self):
        pass

    def to_dict(self):
        self.__compute()
        return [journey.to_dict() for journey in self._journeys]


class Segment():
    def __init__(self, parent_segments, data):
        self.__parent_segments = parent_segments
        self.__parent_journey = parent_segments.journey and parent_segments.journey or False
        self.__dict__ = self.__default()
        self.fares = Fares(self)
        self.legs = Legs(self)
        self.update(data)

    def __default(self):
        return {
            'sequence': self.__parent_segments and len(self.__parent_segments) or 0,
            'journey_type': self.__parent_journey and self.__parent_journey.journey_type or False,
            'segment_code': '',
            'segment_code2': '',
            'origin': '',
            'destination': '',
            'departure_date': '',
            'arrival_date': '',
            'origin_terminal': '',
            'destination_terminal': '',
            'carrier_code': '',
            'carrier_number': '',
            'carrier_name': '',
            'transit_duration': '0h 0m',
            'elapsed_time': '00:00',
            'is_international_flight': 'false',
            'equipment_type': '',
            'meal_code': '',
            'operating_airline_code': '',
            'provider': '',
        }

    def __nonzero__(self):
        return self.segment_code and True or False

    def update(self, new_data):
        self.__dict__.update(new_data)

        # fungsi yang ada belum memperhatikan timezone dari departure dan arrival zone-nya
        # self.__dict__['elapsed_time'] = util.calculate_elapsed_time(self.__dict__['departure_date'], self.__dict__['arrival_date'])

        if not self.__dict__['carrier_name']:
            self.__dict__['carrier_name'] = self.__dict__['carrier_code'] + self.__dict__['carrier_number']

        if not self.__dict__['segment_code']:
            self.__dict__['segment_code'] = util.segment_attr_to_code(self.__dict__, self.__dict__['provider'])

        self.__dict__['operating_airline_code'] = self.__dict__.get('operating_airline_code',
                                                                    self.__dict__['carrier_code'])
        if not self.__dict__['segment_code2']:
            self.__dict__['segment_code2'] = self.__dict__['origin'] + self.__dict__['destination']

    def __compute(self):
        pass

    def to_dict(self):
        self.__compute()
        res = self.__dict__.copy()
        res['fares'] = self.fares.to_dict()
        res['legs'] = self.legs.to_dict()
        return res

    @property
    def segments(self):
        return self.__parent_segments

    @property
    def journey(self):
        return self.__parent_journey

    def add_fare(self, data):
        return self.fares.add_fare(data)

    def add_leg(self, data):
        return self.legs.add_leg(data)


class Segments():
    def __init__(self, parent_journey):
        self.__parent_journey = parent_journey
        self._segments = []

    def __getitem__(self, item):
        return self._segments[item]

    @property
    def journey(self):
        return self.__parent_journey

    def __iter__(self):
        return iter(self._segments)

    def __nonzero__(self):
        return self._segments and True or False

    def __len__(self):
        return len(self._segments)

    def add_segment(self, data={}):
        _new = Segment(self, data)
        self._segments.append(_new)
        return _new

    def add_service_charge(self, segment_index, fare_index, data):
        _default = {
            'sequence': 0,
            'charge_code': 'fare',
            'pax_type': 'ADT',
            'pax_count': 0,
            'amount': 0,
            'currency': 'IDR',
            'foreign_amount': 0,
            'foreign_currency': 'IDR',
            'fare_disc_code': '',
            'pax_disc_code': '',
        }
        _default.update(data)
        _fare_obj = self._segments[segment_index]['fares'][fare_index]
        if _default['charge_code'] == 'fare' and _default['pax_type'] == 'ADT':
            _fare_obj['base_fare'] = _default['amount']
        _fare_obj['service_charges'].append(_default)
        _default['sequence'] = len(_fare_obj['service_charges']) - 1
        return _default

    def __compute(self):
        last_seg = None
        for seg in self._segments:
            if last_seg:
                seg.transit_duration = util.calculate_transit_duration(last_seg.arrival_date, seg.departure_date)
            last_seg = seg

    @property
    def count(self):
        return len(self._segments)

    def to_dict(self):
        self.__compute()
        return [seg.to_dict() for seg in self._segments]


class ItineraryPriceRS():
    def __init__(self):
        self._journeys = []

    def add_journey(self, journey):
        _default = {
            'departure': '',
            'origin': '',
            'departure_date': '',
            'arrival_date': '',
            'direction': '',
            'segments_obj': Segments(),
            'segments': [],
            'service_charge_summary': [],
            'sale_service_charge_summary': [],
            'journey_type': 'DP'
        }
        _default.update(journey)
        self._journeys.append(_default)
        _default['sequence'] = len(self._journeys) - 1
        return _default

    def add_segment(self, journey_idx, segment, provider):
        return self._journeys[journey_idx]['segments_obj'].add_segment(segment, provider)

    def add_fare(self, segment_index, data):
        _default = {
            'sequence': 0,
            'fare_code': '',
            'class_of_service': 'Y',
            'subclass': '',
            'available_count': 0,
            'rule_number': '',
            'product_class': '',
            'product_class_description': '',
            'service_charges': [],
            'base_fare': 0,
            'fare_basis_codes': []
        }
        _default.update(data)
        self._journeys['segments'][segment_index]['fares'].append(_default)
        _default['sequence'] = len(self._journeys['segments'][segment_index]['fares']) - 1
        return _default

    def add_service_charger_object(self, journey_idx, service_charge_summary, sale_service_charge_summary):
        pass

    def __compute(self):
        for jour in self._journeys:
            segments_obj = jour.pop('segments_obj')
            jour['segments'] = segments_obj.get_data()

            # Departure Process
            tmp = self.segment_code_to_dict(jour['segments'][0]['segment_code'])
            jour.update({
                'origin:': tmp['origin'],
                'departure_date:': tmp['departure_date'],
            })

            # Return Process
            tmp = util.segment_code_to_dict(jour['segments'][-1]['segment_code'])
            jour.update({
                'destination:': tmp['destination'],
                'arrival_date:': tmp['arrival_date'],
            })

    def get_data(self):
        self.__compute()
        return self._journeys


class GetPriceItineraryRS():
    def __init__(self):
        self._payload = {
            'journeys': [],
            'carrier_code_list': set()
        }


class GetAvailabilitiesRS():
    def __init__(self):
        self._payload = {
            'journeys': [],
            'carrier_code_list': set()
        }

    def __compute(self):
        self._payload['carrier_code_list'] = list(self._payload['carrier_code_list'])

    def add_journey(self, journey):
        self._payload['carrier_code_list'] = self._payload['carrier_code_list'].union(journey['carrier_code_list'])
        self._payload['journeys'].append(journey)

    @property
    def payload(self):
        self.__compute()
        return self._payload


class GetAvailabilityRS(object):
    def __init__(self):
        self._journey = {}

    @property
    def segments(self):
        return self._journey['segments']

    def add_journey(self, data):
        _default = {
            'sequence': 0,
            'journey_type': 'DP',
            'journey_code': '',
            'provider': '',
            'origin': '',
            'destination': '',
            'departure_date': '',
            'arrival_date': '',
            'return_date': '',
            'IncludesTaxesAndFees': '',
            'transit_count': 0,
            'segments': [],
            'service_charge_summary': [],
            'combo_price': False,
            'carrier_code_list': set()  # at the end of process, we convert to list
        }
        data['sequence'] = data.get('sequence') and data.get('sequence') or len(_default)
        _default.update(data)
        self._journey = _default
        return _default

    def add_segment(self, data, provider=''):
        _default = {
            'sequence': 0,
            'journey_type': self._journey['journey_type'],
            'segment_code': '',
            'origin': '',
            'destination': '',
            'departure_date': '',
            'arrival_date': '',
            'origin_terminal': '',
            'destination_terminal': '',
            'carrier_code': '',
            'carrier_number': '',
            'carrier_name': '',
            'transit_duration': '0h 0m',
            'elapsed_time': '00:00',
            'is_international_flight': 'false',
            'equipment_type': '',
            'meal_code': '',
            'operating_airline_code': '',
            'fares': [],
        }
        _default.update(data)
        if not _default['segment_code']:
            _default['segment_code'] = util.segment_attr_to_code(_default, provider)
        _default['operating_airline_code'] = _default['operating_airline_code'] and _default['operating_airline_code'] \
                                             or _default['carrier_code']
        self._journey['carrier_code_list'] = self._journey['carrier_code_list'].union([data['carrier_code']])
        self._journey['segments'].append(_default)
        _default['sequence'] = len(self._journey['segments']) - 1
        self._journey['journey_code'] = ';'.join((self._journey['journey_code'], _default['segment_code']))
        if self._journey['journey_code'][0] == ';':
            self._journey['journey_code'] = self._journey['journey_code'][1:]
        if self._journey['journey_code'][-1] == ';':
            self._journey['journey_code'] = self._journey['journey_code'][:-1]
        return _default

    def add_fare(self, segment_index, data):
        _default = {
            'sequence': 0,
            'fare_code': '',
            'class_of_service': 'Y',
            'subclass': '',
            'available_count': 0,
            'rule_number': '',
            'product_class': '',
            'product_class_description': '',
            'service_charges': [],
            'base_fare': 0,
            'fare_basis_codes': []
        }
        _default.update(data)
        self._journey['segments'][segment_index]['fares'].append(_default)
        _default['sequence'] = len(self._journey['segments'][segment_index]['fares']) - 1
        return _default

    def add_service_charge(self, segment_index, fare_index, data):
        _default = {
            'sequence': 0,
            'charge_code': 'fare',
            'pax_type': 'ADT',
            'pax_count': 0,
            'amount': 0,
            'currency': 'IDR',
            'foreign_amount': 0,
            'foreign_currency': 'IDR',
            'fare_disc_code': '',
            'pax_disc_code': '',
        }
        _default.update(data)
        _fare_obj = self._journey['segments'][segment_index]['fares'][fare_index]
        if _default['charge_code'] == 'fare' and _default['pax_type'] == 'ADT':
            _fare_obj['base_fare'] = _default['amount']
        _fare_obj['service_charges'].append(_default)
        _default['sequence'] = len(_fare_obj['service_charges']) - 1
        return _default

    def split_DP_RT(self):
        # Fixme : Tidak ada algoritma yg dapat mendeteksi round trip point
        return True

    def is_round_trip(self):
        return self.segments[0]['origin'] == self.segments[-1]['destination']

    def __compute(self):
        # =============== segment : elapsed_time, transit_duration ==================
        last_seg = None
        return_seg = None
        arrival_seg = None
        for seg in self.segments:
            if last_seg:
                seg['transit_duration'] = util.calculate_transit_duration(last_seg['arrival_date'],
                                                                          seg['departure_date'])

                # Fixme : untuk Sabre/Promo_price = promo journye perlu perhatikan, krn dalam satu journey(DP) terdiri dari DP & RT
                if self._journey['combo_price']:
                    if last_seg['journey_type'] != seg['journey_type'] and last_seg['journey_type'] == 'DP':
                        return_seg = seg
                        arrival_seg = last_seg
            last_seg = seg

        # =============== journey : transit_count, departure_date, arrival_date, elapsed_time ========


        # 'Jika combo_price & ada return segment
        if return_seg:
            self._journey.update({
                'origin': self.segments[0]['origin'],
                'destination': arrival_seg['destination'],
                'transit_count': arrival_seg['sequence'],
                'arrival_date': arrival_seg['arrival_date'],
                'return_date': return_seg['departure_date'],
            })

        # carrier_code_list : dict to list
        self._journey['carrier_code_list'] = list(self._journey['carrier_code_list'])

    def get_data(self):
        self.__compute()
        return self._journey


class ServiceChargeSummaryRS():
    def __init__(self):
        self.__dict__ = []
        self.__dict___group_pax = {}
        self.flight_code = ''

        # setattr(self, 'data', self.get_data())

    def new_service_charge(self, data):
        _default = {
            'sequence': 0,
            'charge_code': 'fare',
            'pax_type': 'ADT',
            'pax_count': 0,
            'amount': 0,
            'currency': 'IDR',
            'foreign_amount': 0,
            'foreign_currency': 'IDR',
            'fare_disc_code': '',
            'pax_disc_code': '',
        }
        _default.update(data)
        return _default

    def set_flight_code(self, flight_code):
        self.flight_code = flight_code

    def add_service_charge(self, data):
        _default = {
            'pax_type': 'ADT',
            'charge_code': '',
            'charge_type': '',
            'amount': 0,
            'pax_count': 0,
            'currency': 'IDR',
            'foreign_currency': 'IDR',
            'foreign_amount': 0,
            'description': '',
        }
        _default.update(data)
        self.__dict__.append(_default)
        _default['sequence'] = len(self.__dict__) - 1
        pax_type = _default['pax_type']
        charge_code = _default['charge_code']

        if self.__dict___group_pax.get(pax_type):
            if self.__dict___group_pax[pax_type].get(charge_code):
                self.__dict___group_pax[pax_type][charge_code]['amount'] += _default['amount']
                self.__dict___group_pax[pax_type][charge_code]['foreign_amount'] += _default['foreign_amount']
            else:
                self.__dict___group_pax[pax_type].update({charge_code: _default})
        else:
            self.__dict___group_pax[pax_type] = {charge_code: _default}

    def __normalisasi_structure(self):
        # Normalisasi Structure : key word menjadi 'pax_type'
        service_chg_sum = []
        service_chg_sum_sale = []
        for pax_type in self.__dict___group_pax.keys():
            tmp_scs = {
                'pax_type': pax_type,
                'flight_code': self.flight_code,
                'service_charges': [],
            }
            tmp_scs_sale = {
                'pax_type': pax_type,
                'flight_code': self.flight_code,
                'service_charges': [],
            }
            service_chg_sum.append(tmp_scs)
            service_chg_sum_sale.append(tmp_scs_sale)

            amount_fare, amount_inf, amount_tax, foreign_amount_fare, foreign_amount_inf, foreign_amount_tax = 0, 0, 0, 0, 0, 0

            for charge_code in self.__dict___group_pax[pax_type].keys():
                tmp_sc = self.__dict___group_pax[pax_type][charge_code]
                tmp_scs['service_charges'].append(tmp_sc)
                # Proses summary for Sale Service Charge
                if charge_code == 'fare':  # i.sc = INF Service Charge
                    amount_fare += tmp_sc['amount']
                    foreign_amount_fare += tmp_sc['foreign_amount']
                elif charge_code == 'inf':  # i.sc = INF Service Charge
                    amount_inf += tmp_sc['amount']
                    foreign_amount_inf += tmp_sc['foreign_amount']
                else:
                    amount_tax += tmp_sc['amount']
                    foreign_amount_tax += tmp_sc['foreign_amount']

            if amount_fare:
                tmp_scs_sale['service_charges'].append(self.new_service_charge(
                    {'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'fare',
                     'amount': amount_fare, 'foreign_amount': foreign_amount_fare}
                ))
            if amount_inf:
                tmp_scs_sale['service_charges'].append(self.new_service_charge(
                    {'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'inf',
                     'amount': amount_inf, 'foreign_amount': foreign_amount_inf}
                ))
            if amount_tax:
                tmp_scs_sale['service_charges'].append(self.new_service_charge(
                    {'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'tax',
                     'amount': amount_tax, 'foreign_amount': foreign_amount_tax}
                ))

                # tmp_scs['service_charges'] = sorted(tmp_scs['service_charges'], key=lambda k: k['sequence'])
        return service_chg_sum, service_chg_sum_sale
        # return service_chg_sum, service_chg_sum

    @property
    def summary(self):
        return self.__normalisasi_structure()

    def get_data_non_group(self):
        return self.__dict__

        # def summary(self):
        #     return self.__normalisasi_structure()


class GetBookingRS():
    def __init__(self):
        self.paylod = {}

    def __default(self):
        return {
            'pnr': '',
            'provider': '',
            'total_orig': 0,
            'total': 0,
            'currency': 'IDR',
            'expired_date': '',
            'status': '',
            'hold_date': '',
            'passengers': [],
            'contacts': [],
            'segments': [],
            'service_charge_summary': [],
            'sale_service_charge_summary': []
        }

    def new_data(self, booking_data):
        self.paylod = self.__default()
        self.paylod.update(booking_data)

    def add_contact(self, contacts):
        if type(contacts) != list:
            contacts = [contacts]
        for contact in contacts:
            _default = {
                'title': '',
                'first_name': '',
                'last_name': '',
                'email': '',
                'mobile': '',
                'work_phone': '',
                'home_phone': '',
                'other_phone': '',
                'address': '',
                'city': 'Surabaya',
                'province_state': '',
                'country_code': 'ID',
                'postal_code': '',
                'contact_id': 0,
                'sequence': 0,
            }
            _default.update(contact)
            self.paylod['contacts'].append(_default)
            _default['sequence'] = len(self.paylod['contacts'])

    def add_passengers(self, passangers):
        if type(passangers) != list:
            passangers = [passangers]
        for psg in passangers:
            _default = {
                'pax_type': 'ADT',
                'title': 'Mr',
                'first_name': '',
                'last_name': '',
                'gender': 'Male',
                'nationality_code': '',
                'birth_date': '',
                'passport_number': '',
                'passport_expdate': '',
                'country_of_issued_code': '',
                'passenger_ssrs_request': '',
                'pax_baggage': '',
                'ff_airline': '',
                'ff_number': '',
                'passenger_id': '',
                'sequence': 0
            }
            _default.update(psg)
            self.paylod['passengers'].append(_default)
            _default['sequence'] = len(self.paylod['passengers'])

    def add_segment(self, segments):
        if type(segments) != list:
            segments = [segments]
        for seg in segments:
            _default = {
                'pax_type': 'ADT',
                'title': 'Mr',
                'first_name': '',
                'last_name': '',
                'gender': 'Male',
                'nationality_code': '',
                'birth_date': '',
                'passport_number': '',
                'passport_expdate': '',
                'country_of_issued_code': '',
                'passenger_ssrs_request': '',
                'pax_baggage': '',
                'ff_airline': '',
                'ff_number': '',
                'passenger_id': '',
                'sequence': 0
            }
            _default.update(seg)
            self.paylod['segments'].append(_default)
            _default['sequence'] = len(self.paylod['segments'])


class CreateBookingRS():
    def __init__(self):
        self.paylod = {}

    def __default(self):
        return {
            'pnr': '',
            'hold_date': '',
            'status': 'BOOKED',
            'currency': 'IDR',
            'balance_due': 0,
            'total': 0,
        }

    def new_data(self, data_dict):
        self.paylod = self.__default()
        self.paylod.update(data_dict)

class ServiceCharges():
    def __init__(self, parent_fare):
        self.__parent_fare = parent_fare
        self._service_charges = []
        self.__dict__group_pax = {}

    def __iter__(self):
        return iter(self._service_charges)

    def __getitem__(self, item):
        return self._service_charges[item]

    def __nonzero__(self):
        return self._service_charges and True or False

    @property
    def count(self):
        return len(self._service_charges)

    @property
    def fare(self):
        return self.__parent_fare

    def add_service_charge(self, data):
        new_sc = ServiceCharge(self, data)
        self._service_charges.append(new_sc)

        pax_type = new_sc.pax_type
        charge_code = new_sc.charge_code

        if self.__dict__group_pax.get(pax_type):
            if self.__dict__group_pax[pax_type].get(charge_code):
                self.__dict__group_pax[pax_type][charge_code]['amount'] += new_sc.amount
                self.__dict__group_pax[pax_type][charge_code]['foreign_amount'] += new_sc.foreign_amount
            else:
                self.__dict__group_pax[pax_type].update({charge_code: new_sc.to_dict()})
        else:
            self.__dict__group_pax[pax_type] = {charge_code: new_sc.to_dict()}

        return new_sc

    def to_dict(self):
        return [sc.to_dict() for sc in self._service_charges]

    def summary(self):
        service_chg_sum = []
        service_chg_sum_sale = []
        for pax_type in self.__dict__group_pax.keys():
            tmp_scs = {
                'pax_type': pax_type,
                'service_charges': ServiceCharges(False),
            }
            tmp_scs_sale = {
                'pax_type': pax_type,
                'service_charges': ServiceCharges(False),
            }
            service_chg_sum.append(tmp_scs)
            service_chg_sum_sale.append(tmp_scs_sale)

            amount_fare, amount_inf, amount_tax, amount_rac, foreign_amount_fare, foreign_amount_inf, foreign_amount_tax, foreign_amount_rac = 0, 0, 0, 0, 0, 0, 0, 0

            for charge_code in self.__dict__group_pax[pax_type].keys():
                tmp_sc = self.__dict__group_pax[pax_type][charge_code]
                tmp_scs['service_charges'].add_service_charge(tmp_sc)
                # Proses summary for Sale Service Charge
                if charge_code == 'fare':  # i.sc = INF Service Charge
                    amount_fare += tmp_sc['amount']
                    foreign_amount_fare += tmp_sc['foreign_amount']
                elif charge_code == 'inf':  # i.sc = INF Service Charge
                    amount_inf += tmp_sc['amount']
                    foreign_amount_inf += tmp_sc['foreign_amount']
                elif charge_code == 'r.ac':
                    amount_rac += tmp_sc['amount']
                    foreign_amount_rac += tmp_sc['foreign_amount']
                else:
                    amount_tax += tmp_sc['amount']
                    foreign_amount_tax += tmp_sc['foreign_amount']

            if amount_fare:
                tmp_scs_sale['service_charges'].add_service_charge({'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'fare',
                     'amount': amount_fare, 'foreign_amount': foreign_amount_fare})
            if amount_inf:
                tmp_scs_sale['service_charges'].add_service_charge({'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'inf',
                     'amount': amount_inf, 'foreign_amount': foreign_amount_inf})
            if amount_tax:
                tmp_scs_sale['service_charges'].add_service_charge({'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'tax',
                     'amount': amount_tax, 'foreign_amount': foreign_amount_tax})

            if amount_rac:
                tmp_scs_sale['service_charges'].add_service_charge({'pax_type': pax_type, 'pax_count': tmp_sc['pax_count'], 'charge_code': 'r.ac',
                     'amount': amount_rac, 'foreign_amount': foreign_amount_rac})

            tmp_scs['service_charges'] = tmp_scs['service_charges'].to_dict()
            tmp_scs_sale['service_charges'] = tmp_scs_sale['service_charges'].to_dict()

        return service_chg_sum, service_chg_sum_sale

    def get_data_non_group(self):
        return self.__dict__

    def set_flight_code(self, flight_code):
        self.flight_code = flight_code

class ServiceCharge():
    def __init__(self, parent_service_charges, data):
        self.__parent_service_charges = parent_service_charges
        self.__dict__ = self.__default()
        self.update(data)

    def __nonzero__(self):
        return self.charge_code and True or False

    def __default(self):
        return {
            'sequence': self.service_charges and len(self.service_charges._service_charges) or 0,
            'charge_code': 'fare',
            'pax_type': 'ADT',
            'pax_count': 0,
            'amount': 0,
            'currency': 'IDR',
            'foreign_amount': 0,
            'foreign_currency': 'IDR',
            'fare_disc_code': '',
            'pax_disc_code': '',
            'total': 0
        }

    def __compute(self):
        self.__dict__['total'] = self.pax_count * self.amount

    def to_dict(self):
        self.__compute()
        res = self.__dict__.copy()
        return res

    def update(self, new_data):
        self.__dict__.update(new_data)
        # self.__compute()

    @property
    def service_charges(self):
        return self.__parent_service_charges

class Contacts():
    def __init__(self):
        self._contacts = []

    def __getitem__(self, item):
        return self._contacts[item]

    def __iter__(self):
        return iter(self._contacts)

    def __nonzero__(self):
        return self._contacts and True or False

    def __len__(self):
        return len(self._contacts)

    @property
    def count(self):
        return len(self._contacts)

    def add_contact(self, data):
        _new = Contact(self, data)
        self._contacts.append(_new)
        return _new

    def to_dict(self):
        return [ctc.to_dict() for ctc in self._contacts]

class Contact():
    def __init__(self, parent_contacts, data):
        self.__parent_contacts = parent_contacts
        self.__dict__ = self.__default()
        self.update(data)

    @property
    def contacts(self):
        return self.__parent_contacts

    def __nonzero__(self):
        return self.first_name and True or False

    def __compute(self):
        pass

    def __default(self):
        return {
            'sequence': self.contacts and len(self.contacts._contacts) or 0,
            'title': '',
            'first_name': '',
            'last_name': '',
            'email': '',
            'mobile': '',
            'work_phone': '',
            'home_phone': '',
            'other_phone': '',
            'address': '',
            'city': 'Surabaya',
            'province_state': '',
            'country_code': 'ID',
            'postal_code': '',
            'contact_id': 0,
        }

    def to_dict(self):
        # self.__compute()
        return self.__dict__

    def update(self, new_data):
        self.__dict__.update(new_data)
        # self.__compute()

class Passengers():
    def __init__(self):
        self._passengers = []

    def __getitem__(self, item):
        return self._passengers[item]

    def __iter__(self):
        return iter(self._passengers)

    def __nonzero__(self):
        return self._passengers and True or False

    def __len__(self):
        return len(self._passengers)

    @property
    def count(self):
        return len(self._passengers)

    def add_passenger(self, data):
        _new = Passenger(self, data)
        self._passengers.append(_new)
        return _new

    def to_dict(self):
        return [rec.to_dict() for rec in self._passengers]

class Passenger():
    def __init__(self, parent_passengers, data):
        self.__parent_passengers = parent_passengers
        self.__dict__ = self.__default()
        self.update(data)
        # self.__compute()

    @property
    def passengers(self):
        return self.__parent_passengers

    def __default(self):
        return {
            'sequence': self.passengers and len(self.passengers._passengers) or 0,
            'pax_type': 'ADT',
            'title': 'MR',
            'first_name': '',
            'last_name': '',
            'gender': '',
            'nationality_code': '',
            'birth_date': '',
            'passport_number': '',
            'passport_expdate': '',
            'country_of_issued_code': '',
            'passenger_ssrs_request': '',
            'pax_baggage': '',
            'ff_airline': '',
            'ff_number': '',
            'passenger_id': '',
        }

    def __compute(self):
        male = ['MR', 'MSTR']
        female = ['MISS', 'MS', 'MRS']
        gender = ''
        if self.__dict__['title'] in male:
            gender = 'male'
        elif self.__dict__['title'] in female:
            gender = 'female'

        self.__dict__['gender'] = gender

    def to_dict(self):
        self.__compute()
        return self.__dict__

    def update(self, new_data):
        self.__dict__.update(new_data)
        # self.__compute()

class GetBookingRS2():
    def __init__(self):
        self.__dict__ = self.__default()
        self.segments = Segments(False)
        self.contacts = Contacts()
        self.passengers = Passengers()
        self.service_charges = ServiceCharges(False)

    def __default(self):
        return {
            'pnr': '',
            'provider': '',
            'total_orig': 0,
            'total': 0,
            'currency': 'IDR',
            'expired_date': '',
            'status': '',
            'hold_date': '',
            'service_charge_summary': [],
            'sale_service_charge_summary': []
        }

    def add_contact(self, data):
        return self.contacts.add_contact(data)

    def add_passengers(self, data):
        return self.passengers.add_passenger(data)

    def add_segment(self, data):
        return self.segments.add_segment(data)

    def add_service_charge(self, data):
        return self.service_charges.add_service_charge(data)

    def to_dict(self):
        res = self.__dict__.copy()
        service_charge_summary, sale_service_charge_summary = res.pop('service_charges').summary()
        res.update({
            'segments': self.segments.to_dict(),
            'contacts': self.contacts.to_dict(),
            'passengers': self.passengers.to_dict(),
            'service_charge_summary': service_charge_summary,
            'sale_service_charge_summary': sale_service_charge_summary,
        })
        return res

    def update(self, new_data):
        self.__dict__.update(new_data)