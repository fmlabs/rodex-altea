def get_contact():
    return {
        'title': 'Mr', # Mr, Mstr, Mrs, Ms,
        'first_name': 'First',
        'last_name': 'Last',
        'email':'gmail@yahoo.com',
        'mobile':'080989999', # sometime same as homephone
        'work_phone':'08098999', # Sub-Agent phone or Rodex Other Phone
        'home_phone':'080989999', # sometime same as mobile
        'other_phone':'080989999', # Rodex No
        'address':'jakarta', # jakarta
        'city':'jakarta', # Jakarta
        'province_state':'CGK',
        'country_code':'ID',
        'postal_code' : '012365',
        'contact_id': 0 #0 mengindikasikan contact baru, else, existing contact
    }
def get_fare():    
    return {
        'sequence' : 0,
        'fare_code': '',
        'subclass': '',
        'available_count': 0,
        'rule_number': '',
        'product_class': '',
        'service_charges': [],
        'class_of_service': '',
        'base_fare': '',
        'fare_basis_codes':[],
        'product_class_description': ''
    }

def get_journey():
    return {
        'sequence': 0,
        'journey_type': 'DP',
        'journey_code': '',
        'provider': '',
        'origin': '',
        'destination': '',
        'departure_date': '',
        'arrival_date': '',
        'return_date': '',
        'IncludesTaxesAndFees': '',
        'transit_count': 0,
        'segment':[],
        'service_charge_summary': [],
        'sale_service_charge_summary': [],
        'combo_price': False,
        'carrier_code_list': ''  # at the end of process, we convert to list
        }

def get_journey_code():
    return{

    }

def get_segment():
    return {
        'sequence': 0,
        'md': 0,
        'journey_type': '',
        'segment_code': '',
        'segment_code2': '',
        'origin': '',
        'destination': '',
        'departure_date': '',
        'arrival_date': '',
        'origin_terminal': '',
        'destination_terminal': '',
        'carrier_code': '',
        'carrier_number': '',
        'carrier_name': '',
        'transit_duration': '0h 0m',
        'elapsed_time': '00:00',
        'is_international_flight': 'false',
        'equipment_type': '',
        'meal_code': '',
        'operating_airline_code': '',
        'provider': '',
        'fares':[]
        }

def get_service_charge():
    return {
        'sequence':0,
        'charge_code': 'char', #fare, inf, tax, psc, iwjr sc = Surcharge = Biaya Tambahan ac = Agent Discount incv = Incentive
        'pax_type': 'char',
        'pax_count': 0,
        'amount': 0,
        'currency': 'IDR',
        'foreign_amount': 0,
        'foreign_currency': '',
        'fare_disc_code': '',
        'pax_disc_code': 'char'
}

def get_service_charge_summary():
    return {
        'pax_type':'',
        'service_charges':[],
        'flight_code':''
}

def get_passenger():
    return {
        'pax_type':'', #ADT=Adult, CHD=Child, INF=Infant
        'title':'', #Mr, Mstr, Mrs, Ms,
        'first_name': '',
        'last_name': '',
        'gender':'', #Male, Female
        'mobile':'',
        'nationality_code': '',
        'birth_date': '', #yyyy-mm-dd
        'passport_number':'',
        'passport_expdate':'', #yyyy-mm-dd
        'country_of_issue_code': '',
        'segment_ssrs_request':[], #list of pax_ssr per segment object
        'pax_baggage':[], #list of pax_bag per segment
        'ff_airline':'', #frequent flyer doc. type , mis.: Garuda, China Airline
        'ff_number':'',
        'passenger_id':0,
        'sequence':0,
        'seat_numbers':''
}

def get_prices_itinerary():
    return {
        'journey_type':'', #DP, RT, CT, AT=ALL TRIP (Sabre)
        'journey_code':'',
        'origin':'',
        'destination':'',
        'departure_date':'',
        'arrival_date':'',
        'service_charge_summary':[], # list of service charge summary
        'sale_service_charge_summary':[],
        'segments':[],
        'product_class_description':''
}

def get_prices_itinary_provider():
    return {
        'provider': '',
        'prices_itinerary':[] #list of prices itinerary
    }
