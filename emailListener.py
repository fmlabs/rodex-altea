import imaplib
import email
import base64
import sys
from lxml import html, etree

def getCodeEmail(user, password):
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(user,password)
    mail.list()
    mail.select('inbox')

    result, data = mail.search(None,'ALL')

    #retrieves the latest (newest) email by sequential ID
    ids = data[0]
    id_list = ids.split()
    latest_email_id = id_list[-1]
    # print(latest_email_id)
    typ, email_data = mail.fetch(id_list[-1], '(RFC822)' )

    raw_email = email_data[0][1]
    # print(type(raw_email))
    raw_email_string = raw_email.decode('utf-8')
    email_message = email.message_from_string(raw_email_string)
    
    for part in email_message.walk():
        email_subject = part['subject']
        email_from = part['from']
        # print(email_subject)
        # print(email_from)
        body = part.get_payload()
        if type(body) == str:
            # print(body)
            tree = html.fromstring(body)
            result = tree.xpath('//tr/td/div/text()')
            # print(result[0])
            return(result[0])
        # if part.get_content_type() == "text/plain": # ignore attachments/html
        #     body = part.get_payload(decode=True)
        #     print(body.decode('utf-8'))

        # else:
        #     body = part.get_payload()
        #     print(body)
        #     continue


if __name__=="__main__":
    # user = sys.argv[1]
    # password = sys.argv[2]
    user = 'reservationrodex@gmail.com'
    password = 'rodex0909'
    readEmail(user, password)