from dataformat import *
import json
import datetime
import re

def segment_parser(data, md):
    listData = data.split('\n')
    del listData[-1]
    # print(listData)
    journeys = []
    segments = []
    for i in range(len(listData)):
        segment = get_segment()
        if i > 0 and 'GARUDA INDONESIA' not in listData[i] and 'NO MORE LATER' not in listData[i]:
            splitedData = " ".join(listData[i].split())
            splitedData = splitedData.replace('+', ' ')
            splitedData = splitedData.strip().split(' ')
            # print(splitedData)
            segment = get_segment()
            segment['fares'] = []
            # check data length
            if len(splitedData) > 9:
                # check if it is line number or not
                if splitedData[0].isdigit():
                    lineNumber = splitedData[0]
                    
                    # check flight code length
                    if len(splitedData[1]) > 2:
                        carrierName =  splitedData[1]
                        carrierCode = 'GA'
                        carrierNumber = carrierName.strip(carrierCode)
                        index = 2
                    else:
                        carrierCode = splitedData[1] 
                        carrierNumber = splitedData[2]
                        carrierName = carrierCode + ' ' + carrierNumber
                        index = 3

                elif len(splitedData[0]) > 2:
                    lineNumber = segments[-1]['sequence']
                    carrierName =  splitedData[0]
                    carrierCode = 'GA'
                    carrierNumber = carrierName.strip(carrierCode)
                    carrier_name = carrierCode + ' ' + carrierNumber
                    index = 1
                else:
                    lineNumber = segments[-1]['sequence']
                    carrierCode = splitedData[0] 
                    carrierNumber = splitedData[1]
                    carrierName = carrierCode + ' ' + carrierNumber
                    index = 2

                for i in range(index, len(splitedData), 1):
                    fare = get_fare()
                    if splitedData[i][0] == '/':
                        origin = splitedData[i].strip('/')
                        if len(splitedData[i+1]) > 1:
                            origin_terminal = ''
                            destination = splitedData[i+1]
                            if len(splitedData[i+2]) > 1:
                                destination_terminal = ''
                                departure_date = splitedData[i+2]
                                arrival_date = splitedData[i+3]
                                planeType = splitedData[i+4]
                            else:
                                destination_terminal = splitedData[i+2]
                                departure_date = splitedData[i+3]
                                arrival_date = splitedData[i+4]
                                planeType = splitedData[i+5]
                        else:
                            origin_terminal = splitedData[i+1]
                            destination = splitedData[i+2]
                            if len(splitedData[i+3]) > 1:
                                destination_terminal = ''
                                departure_date = splitedData[i+3]
                                arrival_date = splitedData[i+4]
                                planeType = splitedData[i+5]
                            else:
                                destination_terminal = splitedData[i+3]
                                departure_date = splitedData[i+4]
                                arrival_date = splitedData[i+5]
                                planeType = splitedData[i+6]

                        departure_date = '{}-{}-{} {}'.format(departure_year, departure_month, departure_day, departure_date[:2] + ':' + departure_date[-2:])
                        arrival_date = '{}-{}-{} {}'.format(departure_year, departure_month, departure_day, arrival_date[:2] + ':' + arrival_date[-2:])
                        segment_code = generate_segment_code(carrierCode,carrierNumber, origin, destination, departure_date, arrival_date)
                        segment['sequence'] = lineNumber
                        segment['md'] = md
                        segment['segment_code'] = segment_code
                        segment['origin'] = origin
                        segment['destination'] = destination
                        segment['departure_date'] = departure_date
                        segment['arrival_date'] = arrival_date
                        segment['origin_terminal'] = origin_terminal
                        segment['destination_terminal'] = destination_terminal
                        segment['carrier_name'] = carrierName
                        segment['carrier_code'] = carrierCode
                        segment['carrier_number'] = carrierNumber
                        segment['transit_duration'] = ''
                        segment['operating_airline_code'] = planeType
                        segments.append(segment)
                        break
                    else:
                        flightClass = splitedData[i][0]
                        availableSeat = splitedData[i][1]
                        fare['subclass'] = flightClass
                        fare['available_count'] = availableSeat
                        segment['fares'].append(fare)
            else:
                if splitedData != [''] and 'ALT*ORIG' not in splitedData:
                    for i in splitedData:
                        flightClass = i[0]
                        availableSeat = i[1]
                        fare['subclass'] = flightClass
                        fare['available_count'] = availableSeat
                        segment['fares'].append(fare)
                        segments[-1]['fares'].append(fare)
        elif 'GARUDA INDONESIA' in listData[i]:
            splitedData = " ".join(listData[i].split())
            splitedData = splitedData.replace('+', ' ')
            splitedData = splitedData.strip().split(' ')
            departure_date = splitedData[-2]
            departure_month = departure_date[2:]
            departure_day = departure_date[:-3]
            now = datetime.datetime.now()
            departure_year = now.strftime('%Y')
            date = datetime.datetime.strptime(departure_day+departure_month, '%d%b')
            departure_day = date.strftime('%d')
            departure_month = date.strftime('%m')
            print(departure_year, departure_day, departure_month)
        
    return segments


def journey_parser(segments):
    journeys = []
    for i in segments:
        # print(i)
        journey = get_journey()
        if len(journeys) > 0:
            # transit flight
            if int(i['sequence']) == int(journeys[-1]['segment'][-1]['sequence']):
                print('transit')
                journeys[-1]['destination'] = i['destination']
                journeys[-1]['segment'].append(i)
                journeys[-1]['return_date'] = i['departure_date']
                journey['destination'] = i['destination']
                journeys[-1]['transit_count'] = len(journeys[-1]['segment']) -1
            else:
                journey['md'] = i['md']
                journey['journey_code'] = i['segment_code']
                journey['provider'] = 'GARUDA'
                journey['sequence'] = int(i['sequence'])
                journey['origin'] = i['origin']
                journey['destination'] = i['destination']
                journey['departure_date'] = i['departure_date']
                journey['arrival_date']= i['arrival_date']
                journey['segment'].append(i)
                journeys.append(journey)    
        else:
            journey['md'] = i['md']
            journey['journey_code'] = i['segment_code']
            journey['provider'] = 'GARUDA'
            journey['sequence'] = int(i['sequence'])
            journey['origin'] = i['origin']
            journey['destination'] = i['destination']
            journey['departure_date'] = i['departure_date']
            journey['arrival_date']= i['arrival_date']
            journey['segment'].append(i)
            journeys.append(journey) 

    return journeys
        

def getPrice(data):
    listData = data
    # print(listData)
    splitedData = " ".join(listData[0].split())
    splitedData = splitedData.replace(" ", "").split('*')
    print(splitedData)
    if len(splitedData[9]) > 1:
        print(splitedData[9])
        return splitedData[9]
    else:
        print(splitedData[10])
        return splitedData[10]


def service_charge_parser(data):
    listData = data
    service_charges = []
    # print(listData)
    for i in listData:
        data = " ".join(i.split())
        data = data.split(' ')
        # print(data)
        if data[0] == 'IDR':
            service_charge = {
                'sequence':0,
                'charge_code': 'char', #fare, inf, tax, psc, iwjr sc = Surcharge = Biaya Tambahan ac = Agent Discount incv = Incentive
                'pax_type': 'char',
                'pax_count': 0,
                'amount': 0,
                'currency': 'IDR',
                'foreign_amount': 0,
                'foreign_currency': '',
                'fare_disc_code': '',
                'pax_disc_code': 'char'
            }
            if len(data) > 2:
                service_charge['charge_code'] = 'fare'
                service_charge['amount'] = data[1]
                service_charges.append(service_charge)
            elif 'YR' in data[1]:
                service_charge['charge_code'] = 'YR'
                service_charge['amount'] = data[1].strip('YR')
                service_charges.append(service_charge)
            elif 'D5' in data[1]:
                service_charge['charge_code'] = 'D5'
                service_charge['amount'] = data[1].strip('D5')
                service_charges.append(service_charge)
            elif 'ID' in data[1]:
                service_charge['charge_code'] = 'ID'
                service_charge['amount'] = data[1].strip('ID')
                service_charges.append(service_charge)
            else:
                pass
                # price['TOTAL'] = data[1]
    # print(service_charges)
    return service_charges

def get_booking_code(response):
    try:
        code = response.split('\n')
        code = " ".join(code[1].split())
        code = code.split(' ')
        code = code[3]
        return code
    except:
        return 'failed'

def get_booking_info(response):
    try:
        r_list = []
        data = response.split('\n')
        for i in range(len(data)):
            if 'OPW' in data[i]:
                opw = data[i] + ' ' + data[i+1]
                r_list.append(opw)
            if 'OPC' in data[i]:
                r_list.append(data[i])

        opw = ' '.join(r_list[0].split())
        opw = opw.split(' ')
        opw = opw[-1]
        opw_day = opw[:2]
        opw_month = opw[2:5]
        opw_time = '{}:{}'.format(opw[6:8], opw[8:10])

        now = datetime.datetime.now()
        expired_time = datetime.datetime.strptime('{}{}{}'.format(now.strftime('%Y'), opw_month, opw_day),'%Y%b%d')
        expired_date = '{}-{}-{} {}'.format(expired_time.strftime('%Y'), expired_time.strftime('%m'), expired_time.strftime('%d'), opw_time ) 
        
        opc = ' '.join(r_list[1].split())
        opc = opc.split(' ')
        opc = opc[1]
        opc_day = opc[4:6]
        opc_month = opc[6:9]
        opc_time = '{}:{}'.format(opc[10:12], opc[12:14])
        hold_time = datetime.datetime.strptime('{}{}{}'.format(now.strftime('%Y'), opc_month, opc_day),'%Y%b%d')
        hold_date = '{}-{}-{} {}'.format(hold_time.strftime('%Y'), hold_time.strftime('%m'), hold_time.strftime('%d'), opc_time ) 
        
        # hold_data =
        time_limit = {}
        time_limit['expired_time'] = expired_date
        time_limit['hold_time'] = hold_date 
        print(time_limit)
        return time_limit
                 
    except:
        pass

def parse_date(date, full=None):
    parsed_date = datetime.datetime(int(date[:4]), int(date[5:7]), int(date[8:10])) 
    if full:
        return '{}{}{}'.format(parsed_date.strftime('%d'), parsed_date.strftime('%b'), parsed_date.strftime('%y'))
    else:
        return '{}{}'.format(parsed_date.strftime('%d'), parsed_date.strftime('%b'))


def get_total_charge(service_charges):
    total = 0
    for i in service_charges: total+=int(i['amount'])
    print(total)
    return total

def generate_segment_code(carrier_code, carrier_number, origin, destination, departure_date, arrival_date, reserved1=None, reserved2=None, reserved3=None, reserved4=None, provider_code='GARUDA'):
    segment_code = ['' for i in range(11)]
    segment_code[0] = carrier_code
    segment_code[1] = carrier_number
    segment_code[2] = origin
    segment_code[3] = destination
    segment_code[4] = departure_date
    segment_code[5] = arrival_date
    segment_code[6] = reserved1
    segment_code[7] = reserved2
    segment_code[8] = reserved3
    segment_code[9] = reserved4
    segment_code[10] = provider_code
    return str(segment_code).strip('[]')   


def check_booking(response):
    if re.search('GA [0-9][0-9][0-9]+', response):
        return True
    else:
        return False

def check_ticket_line(response):
    list_data = response.split('\n')
    for i in list_data:
        if 'FA PAX' in i:
            data = " ".join(i.split())
            data = data.split(' ')
            break
    
    print(data)
    return data[0]

def get_bassinet_info(response, first_name, last_name):
    list_data = response.split('\n')
    name = 'INF{}/{}'.format(last_name, first_name)
    for i in list_data:
        if name in i:
            i = ' '.join(i.split())
            pax_line = i[0]
            # print('line = ', i[0])
        elif 'OPC' in i:
            segment_line = i.split('/')[-1]
            # print('Segment = ', i[-4:]) 

    return pax_line, segment_line

def check_seat(response):
    list_data = response.split('\n')   
    seat_number = ''
    available_seat = []
    plane_type = list_data[1][-3:]
    init_number = ' '.join(list_data[4].split()).split(' ')
    for i, val in enumerate(list_data):
        if 'SM' not in val:
            data = " ".join(val.split())
            if re.match('[1-9][1-9]', data):
                data = data.split(' ')
                index = 2
                # walk through list of seat info
                while True:
                    seat_data=list_data[i+index]
                    seat_data=' '.join(seat_data.split())
                    # if found delimiter, break
                    if re.match('[1-9][1-9]', seat_data) or 'AVAILABLE' in seat_data:
                        break
                    # if data not empty and wing info, add available seat
                    if seat_data != '' and '<' not in seat_data:
                        # print(seat_data)
                        seat_data = seat_data.split(' ')
                        index_init_number = 0
                        init_seat = init_number[index_init_number]
                        # print(init_number)
                        for x,val in enumerate(seat_data[1]):
                            if data[0][x] == '0':
                                index_init_number+=1
                                init_seat = init_number[index_init_number]
                            # if seat_data[1][x] == '.':
                            seat = {
                                'row':init_seat+data[0][x],
                                'column':seat_data[0],
                                'value':seat_data[1][x]
                                }
                            available_seat.append(seat)
                            # print(seat)
                    index+=1
                break

    return available_seat

def parse_report(response):
    # response = "AGY NO - 15082594              QUERY REPORT 05FEB                  CURRENCY IDR\nOFFICE - SUBGI2175             SELECTION:\nAGENT  - 6466DS                                                     05 FEB 2018\n-------------------------------------------------------------------------------\nSEQ NO A/L DOC NUMBER TOTAL DOC    TAX    FEE   COMM FP PAX NAME AS RLOC   TRNC\n-------------------------------------------------------------------------------\n099973 126 5147300679   1085200 153200      0  27960 CA LESTARI/ DS JBFYPZ TKTT\n099977 126 5147300683   1421000 161000      0  37800 CA NUGROHO/ DS JCN64H TKTT\n099978 126 5147300684    649000 109000      0  16200 CA WATIMIN/ DS JFJPI9 TKTT\n099979 126 5147300685   1454000 164000      0  38700 CA LENNY/TJ DS JCLJI2 TKTT\n099984 126 5147300690   1077000 157000      0  27600 CA KOLANG/E DS SWSIPX TKTT\n099985 126 5147300691   1077000 157000      0  27600 CA KOROMPIS DS SWSIPX TKTT\n099986 126 5147300692   1077000 157000      0  27600 CA TANGKILI DS SWSIPX TKTT\n099987 126 5147300693   1077000 157000      0  27600 CA TANGKILI DS SWSIPX TKTT\n099988 126 5147300694    106200  14200      0   2760 CA PETRA KO DS SWSIPX TKTT\n099989 126 5147300695   1715000 365000      0  40500 CA SIDIK/ER DS JJ4LKA TKTT\n099990 126 5147300696   1092000 172000      0  27600 CA KOLANG/E DS SXITRD TKTT\n099991 126 5147300697   1092000 172000      0  27600 CA KOROMPIS DS SXITRD TKTT\n099992 126 5147300698   1092000 172000      0  27600 CA TANGKILI DS SXITRD TKTT\n099993 126 5147300699   1092000 172000      0  27600 CA TANGKILI DS SXITRD TKTT\n099994 126 5147300700    106200  14200      0   2760 CA PETRA KO DS SXITRD TKTT\n099995 126 5147300701   1578000 198000      0  41400 CA HASANUDD DS JIFE5S TKTT\n099996 126 5147300702   1497500 222500      0  38250 CA MALEPPE/ DS JQV8KF TKTT\n)\u003e"
    if 'NO DATA' not in response:
        list_data = response.split('\n')
        del list_data[-1]
        print(list_data[4])
        list_report_data = []
        for i in range(6, len(list_data)):
            report_data = {}
            data = ' '.join(list_data[i].split())
            data = data.split(' ')
            report_data['sequence_no'] = data[0]
            report_data['kode_maskapai'] = data[1]
            report_data['no_tiket'] = data[2]
            report_data['pnr'] = data[10]
            report_data['as'] = data[9]
            report_data['harga_jual'] = int(data[3])
            report_data['tax'] = int(data[4])
            report_data['basic_fare'] = report_data['harga_jual'] - report_data['tax']
            report_data['komisi'] = report_data['basic_fare'] * 0.03
            report_data['pph'] = report_data['komisi'] * 0.02
            report_data['komisi_jual'] = report_data['komisi'] - report_data['pph']
            report_data['NTA'] = report_data['harga_jual'] - report_data['komisi'] + report_data['pph']
            report_data['NTA+0.2%'] = (report_data['NTA']*0.002) + report_data['NTA']
            report_data['komisi_altea+0.2%'] = report_data['harga_jual'] - report_data['NTA+0.2%']
            list_report_data.append(report_data)
            # print(data)

        return list_report_data
    else:
        return None


def get_line_number(resp, subclass, direction):
    list_data = resp.split('\n')
    del list_data[0]
    data_list = []
    for i in list_data:
        a = ' '.join(i.split())
        a = a.split(' ')
        if a[3] == subclass:
            data_list.append(a)
    
    if direction == 'RT':
        return data_list[0][0]
    else:
        return data_list[1][0]

def tp_response_parser(resp):
    list_data = resp.split('\r')
    
    f = open('parsed_data', 'w')
    f.close()

    f = open('parsed_data','a')
    
    # remove page info
    for i, val in enumerate(list_data):
        f.write(val)
        if 'PAGE' in val:
            del list_data[i]

    # f.write(str(data))
    f.close()
    # for i in data:
    #     print(data)

if __name__ == "__main__":
    f = open('resp_tp_all', 'r')
    resp = f.read()
    f.close()
    w = tp_response_parser(resp)
    # print(w)