import os, time
import json

# mkdir for linux
# mkdir /var/log/api_session/
# chown odoo:odoo /var/log/api_session/

_path = '/var/log/tour_travel/'

class session_connector(object):
    _cache_limit = 7*60         #in sec
    _session_limit = 7*60    #in sec 7 menit

    def __init__(self, provider):
        # required context attribute : sid
        if not os.path.exists(_path):
            os.mkdir(_path)

        self.path = _path + provider + '/'
        if not os.path.exists(self.path):
            os.mkdir(self.path)

        self.path_cache = self.path + 'cache/'
        self.path_sess = self.path + 'sess/'

        if not os.path.exists(self.path_cache):
            os.mkdir(self.path_cache)

        if not os.path.exists(self.path_sess):
            os.mkdir(self.path_sess)

        self.provider = provider
        self.context = {}   #context harus memiliki attribute sid_connector yang didapat saat logoon

    def sid_connector(self):
        return self.context['sid_connector']

    def _session_file(self):
        # connector session file name sesuai dengan UID & CLIENT_SID
        return ''.join((self.path_sess, self.context['sid'], '_', 'connector', '_', str(self.context['uid']), '.sess'))

    def _cache_file(self, service_name, mode='RESP', extention='.xml'):
        return ''.join((self.path_cache, self.context['sid'], '_',  service_name, '_', mode, '_', str(self.context['co_uid']),
               extention))

    def __read_cache_file(self, file_name, extention='.xml', mode='RESP'):
        _file = open(self._cache_file(file_name, mode, extention), 'r')
        data = _file.read()
        _file.close()
        return data

    def __write_cache_file(self, data, file_name, extention='.xml', mode='RESP'):
        _file = open(self._cache_file(file_name, mode, extention), 'w')
        _file.write(data)
        _file.close()
        return True

    def _check_context(self):
        self.context['sid_connector'] = self.context.get('sid_connector', False)
        #
        # return {
        #     'uid': self.context['uid'],
        #     'sid_connector': self.context['sid_connector'],
        #     'sid': self.context['sid'],
        #     'remote_addr': self.context['remote_addr']
        # }

    def write_session(self, context):
        '''
        context must add attribute 'sid_connector' yang didapat saat logon
        :param context:
        :return:
        '''
        #create or write a session file
        self.context = context
        self._check_context()
        session_file = open(self._session_file(), 'w')
        session_file.write(str(self.context))
        session_file.close()
        return True

    def update_expire(self, context, sid_connector):
        """
        Untuk Write session & Update Modified Date agar Expire Time di normalisasi
        :return:
        """
        context['sid_connector'] = sid_connector
        self.context = context
        self._check_context()
        session_file = open(self._session_file(), 'w')
        session_file.write(str(self.context))
        session_file.close()


    def read_session(self, context):
        self.context = context
        self._check_context()
        if not self.is_valid(context):
            return False

        session_file = open(self._session_file(), 'r')
        self.context = eval(session_file.read())
        session_file.close()
        return True

    def _isExpired_session(self):
        return time.time() - os.path.getmtime(self._session_file()) > self._session_limit

    def _isExpired_Response_cache(self, service_name):
        return time.time() - os.path.getmtime(self._cache_file(service_name)) > self._cache_limit

    def is_valid(self, context):
        self.context = context
        if not os.path.exists(self._session_file()):
            return False

        if self._isExpired_session():
            return False
        return True


    def write_XMLResponse_cache(self, service_name, xml, context):
        self.context = context
        self.__write_cache_file(xml, service_name, '.xml', 'RESP')

    def write_XMLRequest_cache(self, service_name, xml, context):
        self.context = context
        self.__write_cache_file(xml, service_name, '.xml', 'REQ')

    def write_JSONResponse_cache(self, service_name, data_dict, context):
        self.context = context
        self.__write_cache_file(json.dumps(data_dict), service_name, '.json', 'RESP')

    def write_JSONRequest_cache(self, service_name, data_dict, context):
        self.context = context
        self.__write_cache_file(json.dumps(data_dict), service_name, '.json', 'REQ')

    def read_XMLRequest_cache(self, service_name, context):
        self.context = context
        return self.__read_cache_file(service_name, '.xml', 'REQ')

    def read_XMLResponse_cache(self, service_name, context):
        self.context = context
        return self.__read_cache_file(service_name, '.xml', 'RESP')

    def read_JSONRequest_cache(self, service_name, context):
        self.context = context
        return json.loads(self.__read_cache_file(service_name, '.json', 'REQ'))

    def read_JSONResponse_cache(self, service_name, context):
        self.context = context
        return json.loads(self.__read_cache_file(service_name, '.json', 'RESP'))

class session_api_client(object):
    _cache_limit = 15 * 60  # in sec
    _session_limit = 15 * 60  # in sec
    _service_attempt_allow = 25

    def __init__(self):
        if not os.path.exists(_path):
            os.mkdir(_path)

        self.path = _path + 'clients/'
        self.path_monitor = _path + 'monitors/'
        if not os.path.exists(self.path_monitor):
            os.mkdir(self.path_monitor)

        if not os.path.exists(self.path):
            os.mkdir(self.path)
        self.context = {
            'uid': 0,
            'sid': '',
            'search_id': 0,
            'remote_addr': ''
        }


    def _session_file(self):
        return ''.join((self.path, 'client_', self.context['sid'], '.sess'))

    def write_search_id(self, search_id):
        self.context['search_id'] = search_id
        session_file = open(self._session_file(), 'w')
        session_file.write(str(self.context))
        session_file.close()

    def _cache_file(self, action_name, extention='.json'):
        return ''.join((self.path, self.context['sid'], '_', action_name, '_', str(self.context['co_uid'])
                , extention))

    def _monitor_file(self, action_name, extention='.json'):
        return ''.join((self.path_monitor, self.context['sid'], '_', action_name, '_', str(self.context['co_uid'])
                , extention))

    def write_JSONCache(self, data, service_name, context):
        self.context = context
        _file = open(self._cache_file(service_name), 'w')
        _file.write(json.dumps(data))
        _file.close()
        return True

    def read_JSONCache(self, service_name, context):
        self.context = context
        _file = open(self._cache_file(service_name), 'r')
        data = _file.read()
        _file.close()
        return json.loads(data)

    def _check_context(self):
        if not self.context.get('search_id'):
            self.context['search_id'] = 0

    def write_session(self, context):
        self.context = context
        self._check_context()
        session_file = open(self._session_file(), 'w')
        session_file.write(str(self.context))
        session_file.close()

    def is_valid(self, context):
        self.context = context
        if not os.path.exists(self._session_file()):
            return False

        if self._isExpired_session():
            return False
        return True

    def update_timer(self, context):
        """
        Untuk Write session & Update Modified Date agar Expire Time di normalisasi
        :return:
        """
        self.context = context
        self._check_context()
        session_file = open(self._session_file(), 'w')
        session_file.write(str(self.context))
        session_file.close()
        return True

    def read_session(self, sid):
        self.context['sid'] = sid
        if not self.is_valid(self.context):
            return False

        session_file = open(self._session_file(), 'r')
        #Fixme: SyntaxError: unexpected EOF while parsing
        self.context = eval(session_file.read())
        session_file.close()
        return True

    def _isExpired_session(self):
        return time.time() - os.path.getmtime(self._session_file()) > self._session_limit

    def is_malware_attack(self, service_name, context):
        #Cek : 25 detik terakhir, service_name yang sama tidak diijinkan
        self.context = context
        if not os.path.exists(self._cache_file(service_name, '.json')):
            return False
        return time.time() - os.path.getmtime(self._cache_file(service_name)) < self._service_attempt_allow

    def write_monitor_log(self, service_name, data):
        session_file = open(self._monitor_file(service_name), 'w')
        session_file.write(json.dumps(data))
        session_file.close()

