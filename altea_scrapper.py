from login import Login
from cache import Session
from parser import *
import json
import threading, queue
import datetime
import time

class Altea():
    def __init__(self, context=None, user='DSDAMAYANTI.SUBGI2175', password='Jualan17', email='reservationrodex@gmail.com', email_password='rodex0909', with_login=True):
        self.user = user
        self.password = password
        self.email = email
        self.email_password = email_password
        self.context = context

        # initiate login class
        if with_login:
            self.Login = Login(self.user, self.password, self.email, self.email_password)
            self.session , self.jsessionid, self.contextid = self.Login.login()

            # save session data to file
            self.cache = Session(self.session, self.jsessionid, self.contextid)
            self.cache.add_data('jsessionid', self.jsessionid)
            self.cache.add_data('contextid', self.contextid)
    
    def get_availability(self, context, origin, destination, departure_date, direction='OW',  adult=1, infant=0, child=0, class_of_service='ALL', return_date=None, md=0):
        self.flight_data = []
        # set cmd string
        self.departure_date = parse_date(departure_date)
        self.cmd_string = 'AN{}{}{}'.format(self.departure_date, origin, destination)
        if direction != 'OW': 
            self.return_date = parse_date(return_date)
            self.cmd_string+='*{}'.format(self.return_date)
        
        self.send_cmd('IG')
        self.response = self.send_cmd(self.cmd_string) 
        # # print(response)
        # self.count = 0
        # self.segments = segment_parser(self.response, self.count)
        # self.flight_data.append(self.segments)
        for i in range(md):
        # while 'NO MORE LATER' not in self.cmdResp:
        #     self.count+=1    
            self.response = self.send_cmd('MD') 
            # print(self.response)

        self.segments = segment_parser(self.response, md)
        self.flight_data.append(self.segments)
            # print(self.count)


        print('done')
        self.journeys = journey_parser(self.flight_data[0])
        self.cache.add_data('query', self.cmd_string)
        # self.cache.add_data('segments', self.flight_data)
        self.cache.add_data('journeys', self.journeys)
        return self.jsessionid           

    def get_service_charge(self, jsessionid, context, adult=1, child=0, infant=0, promotion_codes_booking=None, **kwargs):      
        self.queueLock = threading.Lock()
        self.workQueue = queue.Queue(100000)

        self.total_passenger = adult+child+infant
        self.cache_data = self.cache.get_data()
        self.segments = self.cache_data['journeys']
        self.query = self.cache_data['query']
        self.cache.add_data('journeys',[])
        self.count = 0
        self.threads = []
        
        # run thread
        # self.threadCount = len(self.segments)
        self.threadCount = len(self.segments)
        self.list_contextid = self.Login.create_session(jsessionid, self.threadCount)
        for i in range(self.threadCount):
            self.thread = Thread(i, self.session, self.workQueue, self.queueLock, jsessionid, self.list_contextid[i], self.query, self.total_passenger, i)
            self.thread.start()
            self.threads.append(self.thread)

        # fill queue
        self.queueLock.acquire()
        for i in self.segments:
            self.workQueue.put(i)
            self.count+=1
        self.queueLock.release()
        
        # wait queue to empty
        while not self.workQueue.empty():
            pass

        # notify thread to exit
        # global exitFlag
        for i in self.threads:
            i.exitFlag =1
        
        for t in self.threads:
            t.join()
            pass

        print('done')

    def get_prices_itinerary(self, context, journeys_booking_by_provider, adult=1, child=0, infant=0, promotion_codes_booking=None, **kwargs):
        self.prices_itinerary_list = []
        self.service_charge_summary_list = []
        self.sale_service_charge_summary_list = []
        for i in journeys_booking_by_provider:
            self.prices_itinerary = get_prices_itinerary()
            self.prices_itinerary['journey_code'] = i['journey_code']
            self.prices_itinerary['origin'] = i['origin']
            self.prices_itinerary['destination'] = i['destination']
            self.prices_itinerary['departure_date'] = i['departure_date']
            self.prices_itinerary['arrival_date'] = i['arrival_date']
            self.prices_itinerary['segments'] = i['segments']
            
            self.service_charge_summary = get_service_charge_summary()
            self.service_charge_summary['pax_type'] = ''
            self.service_charge_summary['service_charges'] = i['segments'][0]['fares'][0]['service_charge']
            self.service_charge_summary_list.append(self.service_charge_summary)

        return self.prices_itinerary_list

    def create_booking(self, journey, contact, passengers, journey_code, paxs, promotion_code, context, **kwargs):
        # print(journey)
        self.departure_date = parse_date(journey['departure_date'])
        self.origin = journey['origin']
        self.destination = journey['destination']
        self.line_number = journey['sequence']
        self.md = journey['md']
        self.subclass = journey['segment'][0]['fares'][0]['subclass']
        self.pax_count = 0
        self.contact = contact
        for key, value in paxs.iteritems(): 
            if key != 'INF': self.pax_count+=int(value)

        # ignore command
        self.send_cmd('IG', self.jsessionid, self.contextid, self.session)

        # get flight available
        self.cmd_string = 'AN{}{}{}'.format(self.departure_date, self.origin, self.destination)
        print(self.cmd_string)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        for i in range(self.md):
            print('MD')
            self.response = self.send_cmd('MD', self.jsessionid, self.contextid, self.session)
        print(self.response)

        # select segment
        self.cmd_string = 'SS{}{}{}'.format(self.pax_count, self.subclass, self.line_number)
        print(self.cmd_string)
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # input passenger name
        # self.cmd_string = 'NM'
        for i in passengers:
            if i['pax_type'] == 'ADT': 
                self.cmd_string = 'NM1{}/{}{}'.format(i['last_name'], i['first_name'], i['title'])
                print(self.cmd_string)
                self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
                print(self.response)
            elif i['pax_type'] == 'CHD': 
                self.cmd_string = 'NM1{}/{}{}(CHD/{})'.format(i['last_name'], i['first_name'], i['title'], parse_date(i['birth_date'], True))
                print(self.cmd_string)
                self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
                print(self.response)
            elif i['pax_type'] == 'INF': 
                self.cmd_string = '1/(INF {}/{}{}/{})'.format(i['last_name'], i['first_name'], i['title'], parse_date(i['birth_date'], True))
                print(self.cmd_string)
                self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
                print(self.response)
            else:
                pass
        # print(self.cmd_string)
        # self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        # print(self.response)

        # input contact info
        # input home phone
        self.cmd_string = 'APH-{} {}'.format(self.contact['province_state'], self.contact['home_phone'])
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # input mobile phone
        self.cmd_string = 'APM-{} {}'.format(self.contact['province_state'], self.contact['mobile'])
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # input business contact
        self.cmd_string = 'APB-{} {}'.format(self.contact['province_state'], self.contact['work_phone'])
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # input Agency contact
        self.cmd_string = 'APA-SUB 62 22 540 5012 RODEX TRAVEL CO CINDY'
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # input email contact
        self.cmd_string = 'APE-{}'.format(self.contact['email'])
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)
        # email notification
        self.cmd_string = 'APN-E+emailaddress'
        self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
        print(self.response)

        # Corporate check
        if promotion_code != 0:
            self.cmd_string = 'PDN/CORGA{}'.format(promotion_code)
            self.response = self.send_cmd(self.cmd_string,self.jsessionid, self.contextid, self.session)
            print(self.response)
    
        # TKOK
        self.response = self.send_cmd('TKOK',self.jsessionid, self.contextid, self.session)
        print(self.response)
        # RFPAX
        self.response = self.send_cmd('RFSYS', self.jsessionid, self.contextid, self.session)
        print(self.response)
        # ER
        for i in range(2):
            print('ER')
            self.response = self.send_cmd('ER',self.jsessionid, self.contextid, self.session)
        self.booking_status = 'BOOKED' if 'finish' not in self.response else 'BOOKCANCELED'
        print(self.response)
        self.booking_code = get_booking_code(self.response)
        self.booking_limit = get_booking_info(self.response)
        print(self.booking_code) 

        self.total_charge = get_total_charge(journey['segment'][0]['fares'][0]['service_charges'])
        # return(self.booking_code)
        return {
            'pnr': self.booking_code,
            'hold_date': self.booking_limit['hold_time'],
            'status': self.booking_status,
            'currency': 'IDR',
            'total': self.total_charge,
            'balance_due':''
        }

    def get_booking(self, context, pnr):
        self.pnr = pnr
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        # print(self.response)
        get_booking_info(self.response)


    def cancel_booking(self, pnr):
        self.pnr = pnr
        # retrieve booking code
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # ignore all book
        self.cmd_string = 'XI'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # add RF name
        self.cmd_string = 'RFSYS'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # confirm
        self.cmd_string = 'ER'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # check book
        if check_booking(self.response):
            return {'status':'book not canceled'}
        else:
            return {'status':'book canceled'}

    
    def issued(self, context, pnr, corporate_code=None):
        self.pnr = pnr
        
        # retrieve booking code
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)
        
        # insert pricing
        if corporate_code: 
            self.corporate_code=corporate_code
            self.cmd_string= 'FXP/R,U*C{}'.format(self.corporate_code)
            self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        else:
            self.response = self.send_cmd('FXP/R,U', self.jsessionid, self.contextid, self.session)
        
        # insert TQT
        self.response = self.send_cmd('TQT', self.jsessionid, self.contextid, self.session)
        # insert Commission
        self.response = self.send_cmd('FM3', self.jsessionid, self.contextid, self.session)
        # insert payment method
        self.response = self.send_cmd('FPCASH', self.jsessionid, self.contextid, self.session)
        # insert RF code
        self.response = self.send_cmd('RFSYS', self.jsessionid, self.contextid, self.session)
        # issued commit
        for i in range(2): self.response = self.send_cmd('TTP/RT', self.jsessionid, self.contextid, self.session)
        print(self.response)
        
        return {
            "error_code": 0, 
            "error_msg": "", 
            "response": {
                "pnr": self.pnr, 
                "status": "issued", 
                "message": "Success",
            }
        }

    def void_issued(self, pnr):
        self.pnr = pnr

        # retrieve booking code
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # check line nomor tiket
        self.line_number = check_ticket_line(self.response)

        # void ticket number
        self.cmd_string = 'TRDC/L{}'.format(self.line_number)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # add RF name
        self.cmd_string = 'RFSYS'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)
        self.cmd_string = 'ER'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # check line nomor tiket
        self.line_number = check_ticket_line(self.response)

        # check void status
        self.cmd_string = 'TWD/L{}'.format(self.line_number)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)
        

    def special_request(self, pnr, requests):
        """
        requests list of requests
        {
            'type': String, pilihan 'vegetarian', 'bassinet','custom'
            'custom_req': String,
            'pax_first_name': String, nama pax yg membutuhkan bassinet
            'pax_last_name': String 
        }
        """

        self.pnr = pnr
        # retrieve booking code
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        for i in requests:
            # vegetarian meal requests
            if i['type'] == 'vegetarian':
                self.cmd_string = 'SR VGML'
                self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
                print(self.response)
            
            elif i['type'] == 'bassinet':
                self.no_pax, self.segment_line = get_bassinet_info(self.response, i['pax_first_name'], i['pax_last_name'])
                self.cmd_string = 'SRBSCT/{}/P{}'.format(self.segment_line, self.no_pax)
                self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
                print(self.response)

            elif i['type'] == 'custom':
                self.custom_req = i['custom_req']
                self.cmd_string = 'OS GA {}'.format(self.custom_req)
                self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
                print(self.response)

        self.cmd_string = 'RFSYS'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)
        self.cmd_string = 'ER'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

    #  Get Seat Map
    def get_seat_map(self, pnr):
        self.pnr = pnr
        # retrieve booking code
        self.cmd_string = 'RT{}'.format(self.pnr)
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)

        # request seat map
        self.cmd_string = 'SM'
        self.response = self.send_cmd(self.cmd_string, self.jsessionid, self.contextid, self.session)
        print(self.response)
        self.available_seat = check_seat(self.response)
        print(self.available_seat)

    # def select_seat_map(self, seat):
    #     self.url = "https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/do/home.servicesModule/services_addSeat;jsessionid={}".format(self.jsessionid)
    #     self.header = {}

    def get_report(self):
        self.account_list = json.load(open('account_list.json','r'))
        self.list_report = []
        for i in self.account_list:
            self.Login = Login(i['user_name'], i['password'], self.email, self.email_password)
            self.session , self.jsessionid, self.contextid = self.Login.login()
            self.response = self.send_cmd(cmd='TJQ', jsessionid=self.jsessionid, contextid=self.contextid, session=self.session)
            print(self.response)
            self.report = parse_report(self.response)
            if self.report:
                for z in self.report:
                    self.list_report.append(z)
            time.sleep(2)

        print(self.list_report)

    def get_ticket_policy(self, origin, destination, subclass, direction):
        # FQDCGKDPS - Basic fare quote display
        self.cmd_string = 'FQD{}{}/R,U'.format(origin, destination)
        self.resp = self.send_cmd(self.cmd_string, jsessionid=self.jsessionid, contextid=self.contextid, session=self.session)
        # f = open('resp_tp','w')
        # f.write(self.resp)
        # f.close()
        self.line_number = get_line_number(self.resp, subclass, direction)

        # FQN10 - display fare notes  from a fare display
        self.cmd_string = 'FQN{}*PE'.format(self.line_number)
        self.resp = self.send_cmd(self.cmd_string, jsessionid=self.jsessionid, contextid=self.contextid, session=self.session)
        # f = open('resp_tp_all','w')
        # f.write(self.resp)
        # f.close()
        
        # RAW String
        self.raw_string = '' 
        self.raw_string+='{}\n'.format(self.resp)

        while True:
            self.cmd_string = 'MD'.format(self.line_number)
            self.resp = self.send_cmd(self.cmd_string, jsessionid=self.jsessionid, contextid=self.contextid, session=self.session)
            self.raw_string+='{}\n'.format(self.resp)
            if 'NO MORE PAGE AVAILABLE' in self.resp:
                break

        f = open('resp_tp_all','w')
        f.write(self.raw_string)
        f.close()

        # self.tp_response = tp_response_parser(self.raw_tp)
        self.tp_response = {
            'segment':'{}-{}'.format(origin, destination),
            'class':subclass,
            'description': self.raw_string
        }
        
        return self.tp_response
       

    def send_cmd(self, cmd, jsessionid=None , contextid=None, session=None):
        if jsessionid and contextid:
            self.jsessionid = jsessionid
            self.contextid = contextid
        if session:
            self.session = session 
        
        self.url = 'https://tc34.resdesktop.altea.amadeus.com/cryptic/apfplus/modules/cryptic/cryptic?SITE=AGAPAIDL&LANGUAGE=GB&OCTX=ARDW_ESSENTIAL,ARDW_PROD_INTER_APAC'
        self.payload = {
            "jSessionId":self.jsessionid,
            "contextId": self.contextid,
            "userId": "DSDAMAYAN0",
            "organization": "GA",
            "officeId": "SUBGI2175",
            "gds": "AMADEUS",
            "tasks":[
                {"type":"CRY",
                "command":{
                    "command":cmd,
                    "prohibitedList":"SITE_JCPCRYPTIC_PROHIBITED_COMMANDS_LIST_1"
                        }
                }
            ]
        }

        self.request_data = { "data": json.dumps(self.payload)}
        # print(self.request_data)
        try:
            self.request = self.session.post(self.url, data=self.request_data)
            self.response = self.request.json() 
            # print(self.response)
            self.cmdResp = self.response['model']['output']['crypticResponse']['response']
            return self.cmdResp
        except Exception as e:
            print(e)
            return False
    

class Thread(threading.Thread):
    exitFlag = 0
    def __init__(self, threadID, session, q, lock, jsessionid, contextid, query, passenger, md=0):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.q = q
        self.lock = lock
        self.md = md
        self.jsessionid = jsessionid
        self.contextid = contextid
        self.query_string = query
        self.passenger = passenger
        self.altea = Altea(with_login=False)
        self.s = session
    
    def run(self):
        print ("Starting Thread {}".format(self.threadID))
        self.get_price( self.q, self.lock, self.md)
        print ("Exiting Trehad {}".format(self.threadID))

    def get_price(self, q, lock, md):
        while not self.exitFlag:
            lock.acquire()
            if not q.empty():
                self.query = q.get()
                # print(self.contextid)
                lock.release()
                self.altea.send_cmd('IG', self.jsessionid, self.contextid, session=self.s)
                self.altea.send_cmd(self.query_string, self.jsessionid, self.contextid, session=self.s) 
                for i in range(int(self.md)):
                    self.altea.send_cmd('MD', self.jsessionid, self.contextid, session=self.s)
                # for i in self.query:
                    # i['md'] = md
                for fare in self.query['segment'][0]['fares']:
                    self.line_number = self.query['sequence']
                    self.subclass = fare['subclass']
                    # print('MD : {}, Context id: {}, Line:{}, Class:{}'.format(self.md, self.contextid, self.line_number, self.subclass))
                    self.response = self.altea.send_cmd('SS{passenger}{subclass}{line}'.format(passenger=self.passenger, subclass=self.subclass, line=self.line_number), self.jsessionid, self.contextid, session=self.s)
                    # print(self.response)
                    self.response = self.altea.send_cmd('FXX/R,U', self.jsessionid, self.contextid, session=self.s)

                    self.response = self.altea.send_cmd('FQQ01', self.jsessionid, self.contextid, session=self.s)
                    self.service_charge = service_charge_parser(self.response.split('\r'))
                    # print(self.md, self.line_number, self.response.split('\r'))
                    # print(self.service_charge)
                    fare['service_charges'] = self.service_charge
                    self.altea.send_cmd('IG', self.jsessionid, self.contextid, session=self.s)
                # print(self.query)
                # self.journeys = journey_parser([self.query])
                self.journeys = self.query
                
                self.cache = Session(self.s, self.jsessionid, self.contextid)
                self.journey_data = self.cache.get_data()
                self.journey_data['journeys'].append(self.journeys)
                self.cache.add_data('journeys', self.journey_data['journeys'])
            else:
                lock.release()