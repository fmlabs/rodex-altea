import requests
from lxml import etree, html
import time
import json
import imaplib
import email
import base64
import sys
import pickle

class Login():
    def __init__(self, user, password, email, email_password):
        self.email = email
        self.email_password = email_password
        self.user = user
        self.password = password
        # url requests for login
        self.login_url = 'https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/do/login.usermanagement_login/loginAction'
        
        # request data
        self.login_data = {
                'flowExKey':'e1s1',
                'ACTION':'LoginAlias',
                'LOCATION_ID':'',
                'COOKIE_STATUS':'',
                'SOURCE':'login',
                'USER_ALIAS': self.user,
                'PASSWORD':self.password,
                'ORGANISATION':'GA',
                'USE_MAIN_OFFICE':'TRUE',
                'defaultOffice':'TRUE',
                'rememberMe':'',
                'SITE':'AGAPAIDL',
                'LANGUAGE':'GB',
                'aria.target':'body',
                'aria.panelId':'1'
                }

        self.request_header = {
            'Connection':'keep-alive',
            'Accept-Encoding':'gzip, deflate, br',
            'Accept-Language':'en-GB,en-US;q=0.9,en;q=0.8',
            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
            'Host':'tc34.resdesktop.altea.amadeus.com',
            'Origin':'https://tc34.resdesktop.altea.amadeus.com',
            'Referer':'https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/init/login?SITE=AGAPAIDL&LANGUAGE=GB&MARKETS=ARDW_ESSENTIAL,ARDW_PROD_INTER_APAC',
            'X-Requested-With': 'XMLHttpRequest'
            # 'Cookies': 'aria_user_profile={"pref":{"full_user_identification":"DSDAMAYANTI.SUBGI2175","OCTX":null,"ORGANIZATION":"GA","OFFICE_ID":"SUBGI2175","AGENT_SIGN":"6466","AGENT_INITIALS":"DS","DUTY_CODE":"GS","FIRST_NAME":"DEWI SURYA","LAST_NAME":"DAMAYANTI","PHONE_NUMBER":"+62315662000","LANGUAGE_PREF":"EN"},"firstName":"","lastName":"","gender":""}'

        }
        # init session
        self.s = requests.Session()

    
    
    def login(self):
        self.s = self.first_login()
        self.email_code = self.get_code_email()
        self.s, self.jsessionid = self.second_login(self.s, self.email_code)
        self.s, self.dcxid = self.third_login(self.s, self.jsessionid)
        print('dcxid : ', self.dcxid)
        print('jsessionid : ', self.jsessionid)
        return self.s, self.jsessionid,self.dcxid

    def create_session(self, jsessionid, session_count):
        print('creating session')
        # self.s = session
        self.dcxid_list = []
        for i in range(session_count):
            self.s, self.enc = self.create_new_context(jsessionid)
            self.s, self.dcxid = self.context_login(jsessionid, self.enc)
            self.dcxid_list.append(self.dcxid)

        print(self.dcxid_list)
        return(self.dcxid_list)

    def first_login(self):
        print('First Step Login...')
        self.response = self.s.post(self.login_url, data=self.login_data, headers=self.request_header)
        self.cookies = self.s.cookies.get_dict()
        self.login_data['LOCATION_ID'] = self.cookies['lss_loc_id']
        # print(self.cookies)
        return self.s

    def second_login(self, session, email_code):
        self.s = session
        self.email_code = email_code
        self.login_data['flowExKey'] = 'e1s2'
        self.login_data['ONE_TIME_PASSWORD'] = self.email_code
        self.login_data['COOKIE_STATUS'] = 'LSS_COOKIE_REJECTED'
        self.login_data['aria.panelId'] = '2'
        del self.login_data['LOCATION_ID']

        self.request = self.s.post(self.login_url, data=self.login_data, headers=self.request_header)
        
        # Parse response requests to text
        self.response = self.request.text
        
        ## process response to get jsession id
        # remove first declaration xml line
        self.parsed_response = self.response.split("\n",1)[1]

        # parsed xml string to etree object
        self.parsed_response = etree.fromstring(self.parsed_response)
        self.parsed_response = self.parsed_response.xpath('//aria-response')

        # get jsessionid
        self.jsessionid = self.parsed_response[0].get('jsessionid')
        return self.s, self.jsessionid

    def third_login(self, session, jsessionid):
        self.s = session
        self.login_url2 = 'https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/do/login.usermanagement_login/loginComplete;jsessionid={}?flowExKey=e1s1&SITE=AGAPAIDL&LANGUAGE=GB&aria.target=body&aria.panelId=2'.format(jsessionid)
        self.request = self.s.get(self.login_url2, headers=self.request_header)
    
        # parse response requests to text
        self.response = self.request.text

        # remove xml declaration
        self.response = self.response.split("\n",1)[1]
        self.tree = etree.fromstring(self.response)
        self.parsed = self.tree.xpath('//templates-init[@moduleId="cryptic"]')
        self.dcxid = self.parsed[0].text
        
        # load string data to json
        self.jsondata = json.loads(self.dcxid)

        # get context id
        self.dcxid = self.jsondata['model']['dcxid']

        return self.s, self.dcxid
  
    def get_code_email(self):
        # initiate email imap
        self.mail = imaplib.IMAP4_SSL('imap.gmail.com')

        # login email
        self.mail.login(self.email,self.email_password)
        
        # get inbox data
        self.mail.list()
        self.mail.select('inbox')
        self.result, self.data = self.mail.search(None,'ALL')

        #retrieves the latest (newest) email by sequential ID
        self.ids = self.data[0]
        self.id_list = self.ids.split()
        self.latest_email_id = self.id_list[-1]
        self.typ, self.email_data = self.mail.fetch(self.id_list[-1], '(RFC822)' )

        # parse latest email data
        self.raw_email = self.email_data[0][1]
        self.raw_email_string = self.raw_email.decode('utf-8')
        self.email_message = email.message_from_string(self.raw_email_string)
        
        # find code access inside email string
        for part in self.email_message.walk():
            self.email_subject = part['subject']
            self.email_from = str(part['from'])
            self.body = part.get_payload()
            if type(self.body) == str:
                self.tree = html.fromstring(self.body)
                self.result = self.tree.xpath('//tr/td/div/text()')
                print(self.result[0])
                return(self.result[0])

    def create_new_context(self, jsessionid):
        # self.s = session
        self.cookies = self.s.cookies.get_dict()   
        self.c1 = self.cookies['lss_loc_id']
        self.c2 = self.cookies['um_jst']    
        self.request_data = {
            # 'flowExKey':'e5s1',
            'initialAction':'newCrypticSession',
            'recordLocator':'[object MouseEvent',
            'ctiAcknowledge':'false',
            'LOG_PARENT_JSESSIONID':jsessionid,
            'waiAria':'false',
            'SITE':'AGAPAIDL',
            'LANGUAGE':'GB',
            'aria.target':'body',
            'aria.panelId':100
            }
        self.request_header = { 
            'Connection':'keep-alive',
            'Accept':'*/*',
            'Cookie':'s_cc=true; um_jst={}; lss_loc_id={}; '.format(self.c2, self.c1),
            'Accept-Encoding':'gzip, deflate, br',
            'Accept-Language':'en-GB,en-US;q=0.9,en;q=0.8',
            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
            }

        self.request_url = 'https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/do/home.taskmgr/UMCreateSessionKey;jsessionid={}'.format(jsessionid)
        self.request = self.s.post(self.request_url, data=self.request_data, headers=self.request_header)
        self.response = self.request.text
        self.response = self.response.split("\n",1)[1]
        self.response = etree.fromstring(self.response)
        self.response = self.response.xpath('//send-event')
        self.json_response = json.loads(self.response[0].text)
        self.enc_code = self.json_response['ENC']
        return self.s, self.enc_code

    def context_login(self, jsessionid, enc):
        # self.s = session
        # print(session.cookies.get_dict())
        self.request_url = 'https://tc34.resdesktop.altea.amadeus.com/app_ard/apf/do/loginNewSession.UM/login'
        self.request_header = { 
                        'Connection':'keep-alive',
                        'Accept':'*/*',
                        'Accept-Encoding':'gzip, deflate, br',
                        'Accept-Language':'en-GB,en-US;q=0.9,en;q=0.8',
                        'Content-Type':'application/x-www-form-urlencoded'
                        }
        self.request_data = {
                        'SITE':'AGAPAIDL',
                        'recordLocator':'[object MouseEvent]',
                        'LOG_PARENT_JSESSIONID':jsessionid,
                        'LANGUAGE':'GB',
                        'MARKETS':'ARDW_ESSENTIAL,ARDW_PROD_INTER_APAC',
                        'ctiAcknowledge':'false',
                        'waiAria':'false',
                        'initialAction':'newCrypticSession',
                        'aria.target':'body.main.s3',
                        'aria.sprefix':'s3',
                        'ENC': enc,
                        'ENCT':1,
                        'SITE':'AGAPAIDL',
                        'LANGUAGE':'GB',
                        'aria.panelId':25
        }
        self.request = self.s.post(self.request_url, data=self.request_data, headers=self.request_header)
        self.response = self.request.text
        self.response = self.response.split("\n",1)[1]
        self.tree = etree.fromstring(self.response)
        self.parsed = self.tree.xpath('//templates-init[@moduleId="cryptic"]')
        self.dcxid = self.parsed[0].text
        # print(dcxid)  
        # load string data to json
        self.jsondata = json.loads(self.dcxid)
        return self.s, self.jsondata['model']['dcxid']